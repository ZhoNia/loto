MON LOTO
===

# Fichiers

- Vue: foo.html
- Controller: script.js

# Lancer le projet

Ouvrir foo.html avec votre navigateur.

# Fonctionnalités

- Effectuer tirage: Tire jusqu'à 5 nombres aléatoires présents dans la carte.
- Nouvelle carte: Recharge une nouvelle carte.
- Affiche si le tirage est gagnant ou non au bout de 5 tirages.
