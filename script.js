class Carton{

    // CONSTRUCTEUR

    constructor(){

        this.tableau = [];
        this.tableauIndexMasques = [];
        this.listeTirage = [];

        for (let i = 0; i < 3; i++) {

          this.tableau[i] = [];

          for (let j = 0; j < 9; j++) {

            let randomNumber = Math.floor(Math.random() * 90) + 1;

            while (this.tableau.flat().includes(randomNumber)) {

              randomNumber = Math.floor(Math.random() * 90) + 1;

            }

            this.tableau[i][j] = randomNumber;

          }

        }

        for (let i = 0; i < 12; i++){

          let randomNumber = Math.floor(Math.random() * (3 * 9));

          while (this.tableauIndexMasques.includes(randomNumber)){

            randomNumber = Math.floor(Math.random() * (3 * 9));

          }

          this.tableauIndexMasques[i] = randomNumber;

        }

    }

    // GETTER TABLEAU (MEME LES INDEX VIDES)

    getTableau(){

      return this.tableau;

    }

    // GETTER TABLEAU (SEULEMENT LES INDEX MASQUES SUR L'AFFICHAGE)

    getIndexMasque(){

      return this.tableauIndexMasques;

    }

    // GETTER LISTE DU TIRAGE EN COURS

    getTirage(){

      return this.listeTirage;

    }

    // GETTER (LIGNE I COLONNE J)

    getTableauValeur(i, j){

      return this.tableau[i][j];

    }

    getIndexValeur(val){

      for (let i = 0; i < 3; i++) {

        for (let j = 0; j < 9; j++) {

          if(this.tableau[i][j] == val){

            return [i, j];

          }

        }

      }

      return null;

    }

    // AJOUTE UN TIRAGE ALEATOIRE

    ajouterTirage(){

      let continuerDeJouer = false;

      this.tableau.flat().forEach(item => {

        if(!this.tableauIndexMasques.includes(item)){

          if(!this.listeTirage.includes(item)){

            continuerDeJouer = true;

          }

        }

      });

      if(continuerDeJouer){

        let random = Math.floor(Math.random() * 90) + 1;

        let estOk = false;

        while(!estOk){

            random = Math.floor(Math.random() * 90) + 1;

            if(this.tableau.flat().includes(random) && !this.listeTirage.includes(random)){

              estOk = true;

            }

        }

        this.listeTirage.push(random);

        let ligne = this.getIndexValeur(random)[0];
        let col = this.getIndexValeur(random)[1];

        if(this.tableau.flat().includes(random) && !this.tableauIndexMasques.includes(ligne * 9 + col)){

            let val = "td" + random;

            document.getElementById(val).style.backgroundColor = "green";
            document.getElementById(val).style.color = "white";

        }

        return true;

      }

      return false;

    }

    // RETOURNE VRAI SI LES 5 NOMBRES TIRES SONT SUR LA CARTE

    isTirageGagnant(){

      let ligne;
      let col;

      for(let i = 0; i < this.listeTirage.length; i++){

        ligne = this.getIndexValeur(this.listeTirage[i])[0];
        col = this.getIndexValeur(this.listeTirage[i])[1];

        if(!this.tableau.flat().includes(this.listeTirage[i]) || this.tableauIndexMasques.includes(ligne * 9 + col)){

          return false;

        }

      }

       return true;

    }

    // RETOURNE VRAI SI L'ELEMENT RECHERCHE EST DANS LE TABLEAU DES INDEX MADSQUES

    isIndexMasque(i, j){

      if(this.tableauIndexMasques.includes(i * 9 + j)){

        return true;

      }else{

        return false;

      }

    }

}

// CREE UN OBJET CARTON ET REMPLIT LE TABLEAU

function table2HTML(str){

  const c = new Carton();

  let tableHTML = "";

  for (let i = 0; i < 3; i++) {

    tableHTML += "<tr>";

    for (let j = 0; j < 9; j++) {

      if (c.isIndexMasque(i, j)){

        tableHTML += "<td id='caseNoire'></td>";

      }else{

        tableHTML += "<td id='td" + c.getTableauValeur(i, j) + "'>" + c.getTableauValeur(i, j) + "</td>";

      }

    }

    tableHTML += "</tr>";

  }

  document.getElementById(str).innerHTML = tableHTML;

  return c;

}

// FONCTION: EFFECTUER UN TIRAGE

function tirage(c, str){

  if(c.ajouterTirage()){

    document.getElementById(str).innerHTML = "Tirage: ";
    document.getElementById(str).innerHTML += c.getTirage();

  }else{

    document.getElementById("resultat").innerHTML = "(C'est terminé)";

  }

}
